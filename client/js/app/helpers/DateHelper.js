class DateHelper {

  constructor() {
    throw new Error("DateHelper não pode ser instanciada");
  }

  static dataParaTexto(data){
    return `${data.getDate()}/${data.getMonth() + 1}/${data.getFullYear()}`;
  }

  static textoParaData(texto){

    if(!/\d{4}-\d{2}-\d{2}/.test(texto)) {
      throw new Error("deve estar no formato yyyy-mm-dd");
    }

    return new Date(...
      texto.split("-").map((item, indice) => {
          //if para arumar o mes porque no obj Date os meses comecam com 0
          if (indice == 1) {
            return item - 1;
            //opcao: ao inves do if usar "return item - indice % 2"
            //o indice 0 do array retorna 0 então não muda a conta
            // o indice 1 (mes, que queremos mexer) vai retornar 1 no módulo de 2
          }
          return item;
        })
      );
    }
}